# language: ru

@smoke
Функция: : Сайт МТСБ

#  @regression @case1
#  Scenario: Авторизация ЛК
#    Given Пользователь открывает сайт "https://www.mtsbank.ru/malomu-biznesu/online-bank/"
#    When Пользователь нажимает "Войти"
#    When Пользователь нажимает "Не помню пароль"
#    Then Проверить текстовое сообщение "Nope"


#  @regression @case2
#  Scenario: Авторизация ЛК
#    Given Пользователь открывает сайт "https://www.mtsbank.ru/malomu-biznesu/online-bank/"
#    When Пользователь нажимает "Войти"
#    When Пользователь вводит телефон "79277751794"
#    When Пользователь вводит пароль "1234"
#    Then Проверить заполнение поля телефон "+7 792 777-51-79"

  @regression @case2
 Структура сценария: Авторизация ЛК
    Дано Пользователь открывает сайт "https://sso.mtsbank.ru/login/msb/auth/"
    Когда Пользователь вводит телефон "<phone>"
    Когда Пользователь вводит пароль "<password>"
    Тогда Проверить заполнение поля телефон "<expectedPhone>"
    Примеры:
      | phone         | password | expectedPhone    |
      | 92 777-51-79  | 123      | +7 927 775-17-9  |
      | 792 777-51-80 | 123        | +7 792 777-51-80 |
