package steps;

import actions.HomePageActions;
import actions.LoginFormActions;
import io.cucumber.java.AfterAll;
import io.cucumber.java.BeforeAll;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java.ru.Дано;
import io.cucumber.java.ru.Когда;
import io.cucumber.java.ru.Тогда;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Order;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import utils.HelperClass;

import java.time.Duration;

public class SetpDefinitions {

    HomePageActions objHomePage = new HomePageActions();
    LoginFormActions objLoginPage = new LoginFormActions();

    @BeforeAll
    public static void startUp() {
        HelperClass.setUpDriver();
    }

    @AfterAll
    @Order(1)
    static void destroy() {
        HelperClass.tearDown();
    }

    @Дано("Пользователь открывает сайт {string}")
    public void openSite(String url) {
        HelperClass.openPage(url);
    }

    @Когда("Проверить текстовое сообщение {string}")
    public void checkMessage(String message) {

    }

    @Когда("Пользователь нажимает кнопку {string}")
    public void пользовательНажимает(String name) {
        objHomePage.clickButtonLogin(name);
    }

    @Когда("Пользователь нажимает {string}")
    public void пользовательНажимает1(String name) {
        objHomePage.clickButtonLogin(name);
    }


    @Когда("Пользователь нажимает ссылку {string}")
    public void пользовательНажимаетСсылку(String arg0) {

    }

    @Когда("Пользователь вводит телефон {string}")
    public void пользовательВводитТелефон(String arg0) {
        objLoginPage.setInputPhone((arg0));
    }

    @Когда("^Пользователь вводит пароль (\\d+)$")
    public void пользовательВводитПароль(int arg0) {
        objLoginPage.setInputPassword(String.valueOf(arg0));

    }

    @Тогда("Проверить заполнение поля телефон {string}")
    public void проверитьЗаполнениеПоляТелефон(String phone) {
        String result = objLoginPage.checkTextPhone(phone);
        Assertions.assertEquals(phone, result);
    }

    @Когда("пользователь вводит {int}")
    public void пользовательВводит(int arg0) {

    }

    @Когда("Пользователь вводит пароль {string}")
    public void inputPassword(String password) {
        objLoginPage.setInputPassword(password);

    }
}
