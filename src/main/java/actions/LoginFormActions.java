package actions;

import dev.failsafe.internal.util.Assert;
import locators.HomePageLocators;
import locators.LoginPageLocators;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.HelperClass;


public class LoginFormActions {

    LoginPageLocators loginPageLocators;

    public LoginFormActions() {
        this.loginPageLocators = new LoginPageLocators();
        PageFactory.initElements(HelperClass.getDriver(), loginPageLocators);
    }

    public void setInputPhone(String name) {
        loginPageLocators.inputPhone.sendKeys(name);
    }

    public void setInputPassword(String name) {
        loginPageLocators.temporaryPassword.sendKeys(name);
    }

    public String checkTextPhone(String name) {
        return loginPageLocators.inputPhone.getAttribute("value");
    }

    private void myAssert(Object expected, Object actual) {
        if (expected.equals(actual)) {
            //
        } else {
            throw new AssertionError();
        }
    }

}
