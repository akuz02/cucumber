package actions;

import locators.HomePageLocators;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.HelperClass;

public class HomePageActions {

    HomePageLocators homePageLocators;

    public HomePageActions() {
        this.homePageLocators = new HomePageLocators();
        PageFactory.initElements(HelperClass.getDriver(), homePageLocators);
    }

    public void clickButtonLogin(String name) {
        homePageLocators.buttonLogin.click();
//        homePageLocators.getDynamicElement(name).click();
    }

}