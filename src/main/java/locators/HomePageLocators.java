package locators;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePageLocators {

//    @FindBy(xpath = "//*[@id=\"__next\"]/div[5]/div[2]/div/div/section[1]/div[1]/a/div")
	// @FindBy(xpath = "//div[contains(text(),'Войти')]")
    public WebElement buttonLogin;

	public WebElement button;

	public WebElement getDynamicElement(String name) {
		button.findElement(By.xpath("//a[contains(text(),"+name+")]"));
		return button;
	}

}