package locators;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPageLocators {

	@FindBy(name = "phoneNumber")
	public WebElement inputPhone;

	@FindBy(name = "temporaryPassword")
	public WebElement temporaryPassword;

}